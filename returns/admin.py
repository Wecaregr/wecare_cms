from django.contrib import admin

from .models import Order, Transaction, MoneyTransfer

admin.site.register(Order)
admin.site.register(Transaction)
admin.site.register(MoneyTransfer)
