import json
import requests
import datetime

class sws:
    webServiceUrl = 'https://wecare.oncloud.gr/s1services'
    appId='1005'
    s1_un='admin'
    s1_pass='wecare2007'
    logresponce=''
    authresponce=''
    clientID=''
    CompanyID=''
    isAuthendicated=False

    
    def Login(self, username, password, appid ):
        self.logresponce=''
        self.isAuthendicated=False
        jsonstr={"service":"login","username":username,"password":password,"appId":appid}
        resp = self.apiCall(jsonstr)
        print(resp)
        if resp['success']:
            self.logresponce=resp

    def Authendicate_comp(self,comp):
        self.isAuthendicated=False
        self.authresponce=''
        if self.logresponce['success']:
            jsonstr={"service":"authenticate",  "clientID":self.logresponce['clientID'], "COMPANY":self.logresponce['objs'][comp]['COMPANY'],"BRANCH": self.logresponce['objs'][comp]['BRANCH'], "MODULE":self.logresponce['objs'][comp]['MODULE'], "REFID":self.logresponce['objs'][comp]['REFID']}
            print(jsonstr)
            resp = self.apiCall(jsonstr)
            print(resp)
            if resp['success']:
                self.clientID=resp['clientID']
                self.isAuthendicated=True
                self.authresponce=resp
                self.CompanyID=self.logresponce['objs'][comp]['COMPANY']

    def Authendicate(self,company, branch, module, refid):
        if self.logresponce['success']:
            jsonstr={"service":"authenticate",  "clientID":self.logresponce['clientID'], "COMPANY":company,"BRANCH": branch, "MODULE":module, "REFID":refid}
            print(jsonstr)
            resp = self.apiCall(jsonstr)
            print(resp)
            if resp['success']:
                self.clientID=resp['clientID']
                self.isAuthendicated=True
                self.authresponce=resp
                self.CompanyID=company

    def Autoauthendicate(self):
        if self.isAuthendicated == False:
            self.Login(self.s1_un,self.s1_pass,self.appId)
            self.Authendicate_comp(0)
            return self.authresponce
        else:
            return True
            
    def __init__(self):
        print(self.Autoauthendicate())

    def getAXQTYS(self, CODE):
        try:
            repinfo = self.getBrowserInfo('MAT_WHOUSE' , '', "ITEM.CODE={0}*".format(CODE))
            print(str(repinfo))
            qtys = self.getBrowserData(repinfo['reqID'], 0, 1)
            print (qtys)
            return  {'success':True,'MTRL':qtys['rows'][0][0],'id':qtys['rows'][0][1],'name':qtys['rows'][0][2],
            '1000':qtys['rows'][0][4], '1001':qtys['rows'][0][5], '1002':qtys['rows'][0][6], 'sum':qtys['rows'][0][7]}
        except Exception as ex:
            return {'success':False,'error':str(ex)}




  

    ######################################################################
    # GETS ORDER ITEMS FROM AN ARRAY OF ORDERIDS
    #####################################################################
    def getORDERITEMS( self, orderid):
        jsonresponce= self.sqldata("ORDER_ITEMS", orderid, self.CompanyID)
        print(jsonresponce)
        return jsonresponce

    ######################################################################
    # GETS ORDERS VIEW
    #####################################################################
    def getORDERS( self, is_print, is_fullytrans):
        jsonresponce= self.sqldata("ORDERS_VIEW", is_print ,  is_fullytrans)
        return jsonresponce

    ######################################################################
    # GETS BUYS
    #####################################################################
    def getBUYS(self,series):
        jsonresponce= self.sqldata("BUYS_LIST", series)
        return jsonresponce

    ######################################################################
    # GETS BUYS
    #####################################################################
    def get_buy_lines(self, findoc):
        jsonresponce= self.sqldata("BUY_LINES", findoc, self.CompanyID)
        return jsonresponce
    
    def get_buy_lines_new(self, findoc, newlines):
        jsonresponce= self.sqldata("BUY_LINES_RAFI", findoc, self.CompanyID, newlines)
        return jsonresponce

	######################################################
	# Gets Quantities
	######################################################
    def getAXQTY(self, MTRL):
        json_data = self.sqldata("ITEM_QTY2", MTRL, self.CompanyID)
        result=[]
        now = datetime.datetime.now()
        #print now.year, now.month, now.day, now.hour, now.minute, now.second
        if json_data['success']:
            if len(json_data['rows'])>0:
                for row in json_data['rows']:
                    # {'WHOUSE': '1000', 'FISCPRD': '2017', 'IMPQTY1': '7866', 'IMPQTY2': '0', 'BILQIMP': '7936', 'IMPVAL': '51002.8', 'EXPQTY1': '7241', 'EXPQTY2': '0', 'BILQEXP': '7241', 'EXPVAL': '46192.8', 'PURQTY': '7608', 'PURVAL': '48944.06', 'SALQTY': '7236', 'SALVAL': '46160.78', 'CONQTY': '0', 'CONVAL': '0', 'PROQTY': '0', 'PROVAL': '0'}
                    if row['FISCPRD']=='{0}'.format(now.year):
                        try:
                            qty = (int(row['IMPQTY1'])-int(row['EXPQTY1']))
                        except:
                            qty='error'
                        result.append({'WHOUSE': row['WHOUSE'], 'QTY':(int(row['IMPQTY1'])-int(row['EXPQTY1'])), 'FISCPRD':row['FISCPRD'], 'IMPQTY1':row['IMPQTY1'], 'BILQIMP':row['BILQIMP'], 'EXPQTY1':row['EXPQTY1'], 'PURQTY':row['PURQTY'], 'SALQTY':row['SALQTY'], 'CONQTY':row['CONQTY']})
                return result
            else:
                return []
        else:
            return []

	##############################################################################################
    # Sends an sql data request
    ################################################################################
    def getprodfromBARCODE( self, code, fetchqty=True):
        jsonresponce= self.sqldata("itemMTRL_from_barcode", code, self.CompanyID)
        if jsonresponce['success'] and len(jsonresponce['rows'])>0 :
            if fetchqty:
                qtyresponce = self.getAXQTY(jsonresponce['rows'][0]['MTRL'])
                print(qtyresponce)
                jsonresponce['rows'][0]['qty'] = qtyresponce
            return jsonresponce['rows'][0]
        else:
            return ''

    def get_order_data( self, code):
         jsonresponce= self.sqldata("get_order_data", code, self.CompanyID)
         print(jsonresponce)
         return jsonresponce

    ##############################################################################################
    # Like query from barcodes
    ################################################################################
    def getprodfromBARCODE_multi( self, code, fetchqty=True):
        #jsonresponce= self.sqldata("itemMTRL_from_barcode_like", code, self.CompanyID)
        jsonresponce= self.sqldata("getMTRL_from_Barcode_substitutes", code, self.CompanyID)
        
        if jsonresponce['success'] and len(jsonresponce['rows'])>0 : 
            if fetchqty:  
                for row in jsonresponce['rows']:
                    row['qty'] = self.getAXQTY(row['MTRL'])                           
            return jsonresponce['rows']
        else:
            return ''

    #######################################################################
    # Sends an sql data request
    ####################################################################
    def getprodfromID(self, code):
        jsonresponce= self.sqldata("itemMTRL_from_code", code, self.CompanyID)
        if jsonresponce['success'] and len(jsonresponce['rows'])>0 :
            #totalcount = jsonresponce['totalcount']
            return jsonresponce['rows'][0]
        else:
            return ''

    #Returns that data of the fields of a specific tables
    #"KEYNAME": "TRDR", "KEYVALUE":47, "RESULTFIELDS":"CODE,NAME,AFM"
    def selectorFields(self, obj, tablename, keyname, keyvalue, resultfields):
        jsonstr={"service": "selectorFields", "clientID": self.clientID, "appId": self.appId, "TABLENAME": tablename , "KEYNAME": keyname, "KEYVALUE": keyvalue,"RESULTFIELDS": resultfields }
        resp = self.apiCall(jsonstr)
        return resp


    #Returns the data from a Selector of the application.
    #"EDITOR": "1|TRDR|TRDR|SODTYPE=13 AND ISPROSP='0'|", "VALUE": "30*"
    def getSelectorData(self, obj, editor, val):
        jsonstr={"service": "getSelectorData", "clientID": self.clientID, "appId": self.appId, "EDITOR": editor , "VALUE": val }
        resp = self.apiCall(jsonstr)
        return resp


    #Deletes the record of a Business Object.
    def delData(self, obj, frm, key):
        jsonstr={"service": "delData", "clientID": self.clientID, "appId": self.appId, "OBJECT": obj , "FORM": frm, "KEY":key }
        resp = self.apiCall(jsonstr)
        return resp


    #Returns all data (or the selected ones from LOCATEINFO) of a record of a Business Object.
    #"KEY":47, "LOCATEINFO":"CUSTOMER:CODE,NAME,AFM;CUSEXTRA:VARCHAR02,DATE01"
    def getData(self, obj, frm, key, locateinfo):
        jsonstr={"service": "getData", "clientID": self.clientID, "appId": self.appId, "OBJECT": obj , "FORM": frm, "KEY":key, "LOCATEINFO": locateinfo }
        resp = self.apiCall(jsonstr)
        return resp

    #Insert or Modify the data of a record in a Business Object identified by a KEY. If the KEY is empty or missing a record is inserted.
    def setData(self, obj, frm, key, data):
        jsonstr={"service": "setData", "clientID": self.clientID, "appId": self.appId, "OBJECT": obj , "FORM": frm, "KEY":key, "data": data }
        resp = self.apiCall(jsonstr)
        return resp

    #Executes the report and returns a reference code for this report (reqID), together with the number of pages.
    def getReportInfo(self, obj, blist, filters):
        jsonstr={"service": "getReportInfo", "clientID": self.clientID, "appId": self.appId, "OBJECT": obj , "LIST": blist,  "FILTERS": filters }
        resp = self.apiCall(jsonstr)
        return resp

    #Returns in HTML format the report page PAGENUM from the report with reference code (reqID).
    def getReportData(self, reqid, pagenum):
        jsonstr={"service": "getReportData", "clientID": self.clientID, "appId": self.appId, "reqID": reqid  , "PAGENUM": pagenum }
        resp = self.apiCall(jsonstr)
        return resp


    #Returns the tables and fields of a specific Business Object with their presentation format
    def getFormDesign(self, obj, frm=""):
        jsonstr={"service": "getFormDesign", "clientID": self.clientID, "appId": self.appId, "OBJECT": obj , "FORM": frm }
        resp = self.apiCall(jsonstr)
        return resp


    #Returns all fields from a specific browser or report dialog, together with their presentation format.
    def getDialog(self, obj, blist=""):
        jsonstr={"service": "getDialog", "clientID": self.clientID, "appId": self.appId, "OBJECT": obj , "LIST": blist }
        resp = self.apiCall(jsonstr)
        return resp


    #Executes a browser and returns a reference code (reqID) for this, together with the browser fields, the number of records and the columns of the browser
    def getBrowserInfo(self, obj, blist, filters):
        jsonstr={"service": "getBrowserInfo", "clientID": self.clientID, "appId": self.appId, "OBJECT": obj , "LIST": blist,  "FILTERS": filters }
        resp = self.apiCall(jsonstr)
        return resp

    #Returns limited (LIMIT) number of records from the browser with reference code (reqID) starting from record START.
    def getBrowserData(self, reqid, start, limit):
        jsonstr={"service": "getBrowserData", "clientID": self.clientID, "appId": self.appId, "reqID": reqid , "START": start,  "LIMIT": limit }
        resp = self.apiCall(jsonstr)
        return resp

    #Returns all fields of a specific table of a specific application Business Object
    def getTableFields(self, obj, table):
        jsonstr={"service": "getTableFields", "clientID": self.clientID, "appId": self.appId, "OBJECT": obj , "TABLE": table	}
        resp = self.apiCall(jsonstr)
        return resp

    #Returns all tables of a specific Business Object
    def getTables(self, obj):
        jsonstr={"service": "getObjectTables", "clientID": self.clientID, "appId": self.appId, "OBJECT": obj }
        resp = self.apiCall(jsonstr)
        return resp

    #Returns all application Business Objects
    def getObjects(self):
        jsonstr={"service": "getObjects", "clientID": self.clientID, "appId": self.appId }
        resp = self.apiCall(jsonstr)
        return resp

    #Execute the SQL with code 'SqlName' from 'SQL Scripts' and get the result data.
    #Params in the SQL Script are declared as follows: ... AND CODE like '{param1}' AND ...
    def sqldata(self, sqlname, param1, param2="", param3=""):
        jsonstr={"service":"SqlData",  "clientID":self.clientID, "appId":self.appId, "SqlName": sqlname, "param1":param1, "param2":param2,  "param3":param3}
        resp = self.apiCall(jsonstr)
        return resp

    #Main http request to the api
    def apiCall(self, datajson):
        headers = {'content-type': 'application/json'}
        response = requests.post(self.webServiceUrl, data=json.dumps(datajson), headers=headers)
        try:
            json_data = json.loads(response.text)
        except:
            json_data = response.text
        return json_data
