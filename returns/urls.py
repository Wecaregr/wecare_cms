from django.urls import path

from . import views
from .views import TransactionListView, TransactionDetailView

urlpatterns = [
    path('', views.index, name='index'),
    path('get_orders/', views.get_orders, name='get_orders'),
    path('new_transaction/', views.insert_new_transaction, name='new_transaction'),
    path('transactions/', TransactionListView.as_view(), name='transaction_list'),
    path('transactions/<int:pk>', TransactionDetailView.as_view(), name='transaction_detail'),
    path('transactions/<int:pk>/edit', views.update_transaction, name='transaction_edit'),
]