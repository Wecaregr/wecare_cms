from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
class Order(models.Model):
    order_id  = models.IntegerField(default=0)
    customer_name = models.CharField(max_length=250)
    order_date = models.DateTimeField('date Order was received')
    invoice_date = models.DateTimeField('date Order was invoiced')
    payment = models.CharField(max_length=250, null=True)
    transport = models.CharField(max_length=250, null=True)
    total_amount = models.FloatField(null=True, blank=True, default=None)
    delivery_cost = models.FloatField(null=True, blank=True, default=None)
    transport_cost = models.FloatField(null=True, blank=True, default=None)
    status = models.IntegerField(default=0)
    fincode = models.CharField(max_length=20, null=True)
    series = models.CharField(max_length=10, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.order_id

    def was_received_recently(self):
        return self.invoice_date >= timezone.now() - datetime.timedelta(days=20)


class Transaction(models.Model):
    CHANGE_ORDER = 'CH'
    CANCEL_ORDER = 'CA'
    EM_ORDER = 'EM'
    XX_ORDER = 'XX'
    NO_SEND = 'NO'
    RETURN_PROD = 'RP'

    TRANSACTION_TYPE_CHOICES = [  
        (CHANGE_ORDER, 'Μεταβολή Παραγγελίας'), 
        (CANCEL_ORDER, 'Ακύρωση'),
        (EM_ORDER, 'EM'),
        (RETURN_PROD, 'Επιστροφή Προϊόντος'),
        (XX_ORDER, 'Χωρίς Χρέωση'),
        (NO_SEND, 'Xωρίς αποστολή'), 
    ]

    NEW_ORDER_STATUS =  [
        ('PE', 'Pending'),
        ('OK', 'Μπήκε Πληρωμένη'), 
        ('CA', 'Canceled'), 
     ]

    FAULT_OF =  [
        ('WA', 'Αποθήκη'),        
        ('TC', 'Τηλεφωνικό Κέντρο'), 
        ('AC', 'ACS'),
        ('GE', 'ΓΕΝΙΚΗ ΤΑΧΥΔΡΟΜΙΚΗ'),
        ('ET', 'Εταιρεία Παραγωγός'), 
        ('OT', 'Άλλο'), 
     ]

    FAULT_TYPE =  [
        ('EL', 'Δεν έχουμε το προϊόν'),
        ('ES', 'Λάθος στοκ στο Site'), 
        ('SP', 'Σπασμένο Δέμα'), 
        ('LA', 'Λαθος προϊόν'),
        ('KA', 'Καθυστερημένη παράδοση'),        
        ('OT', 'Άλλο'), 
     ]

    original_order  = models.CharField(max_length=10, default='0')
    new_order  =  models.CharField(max_length=10, null= True, blank =True)
    trans_type = models.CharField(max_length=2, choices=TRANSACTION_TYPE_CHOICES, default=CHANGE_ORDER,)    
    customer_name = models.CharField(max_length=250, default='No Name')
    original_order_payment = models.CharField(max_length=250, null=True, blank=True)
    
    description = models.TextField(null=True, blank = True)
    status = models.CharField(max_length=2, choices=NEW_ORDER_STATUS, default='PE')
    fault_of = models.CharField(max_length=2, choices=FAULT_OF, null=True, blank=True)
    fault_type = models.CharField(max_length=2, choices=FAULT_TYPE, null=True, blank=True)
    original_amount = models.FloatField(null=True, blank=True, default=0)   
    new_amount = models.FloatField(null=True, blank=True, default=0)
    
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True,null=True)

    def is_pending_transaction(self):
        return self.status == 'PE'
    
    def save_model(self, request, obj, form, change):
        obj.author = request.user
        super().save_model(request, obj, form, change)
    
    def get_absolute_url(self):
        return reverse('transaction_detail', kwargs={'pk': self.pk})


class MoneyTransfer(models.Model):  

    TRANSFER_ACCOUNT =  [
        ('PE', 'ΠΕΙΡΑΙΩΣ'),
        ('EU', 'EUROBANK'), 
        ('AL', 'ALPHABANK'),  
        ('NB', 'ΕΘΝΙΚΗ'),  
        ('PI', 'ΠΙΣΤΩΤΙΚΗ'), 
        ('PI', 'PAYPAL'),  
        ('ME', 'ΜΕΤΡΗΤΑ'),
    ]

    TRANSFER_STATUS =  [
        ('PE', 'Pending'),
        ('OK', 'Completed'), 
        ('CA', 'Canceled'), 
     ]

     

    transaction  = models.ForeignKey(Transaction, on_delete=models.CASCADE, related_name="transaction_order",)
    transfer_account = models.CharField(max_length=2, choices=TRANSFER_ACCOUNT)
    iban = models.CharField(max_length=50, null=True, blank=True)
    bank_name = models.CharField(max_length=250, null=True, blank=True)
    ammount = models.FloatField(default=0)
    comments = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=2, choices=TRANSFER_STATUS)
    transaction_details = models.TextField( null=True, blank=True)

    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.transaction

    def is_pending_transfer(self):
        return self.status =='PE'