from django.http import HttpResponse, HttpResponseRedirect

from django.template import loader
from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView
from .apps import ReturnsLibs
from .models import Transaction
from .forms import Get_orders_form, TransactionForm


class TransactionListView(ListView):
    model = Transaction
    template_name = 'transaction_list.html'
    ordering = ['-id']
class TransactionDetailView(DetailView):
    model = Transaction
    template_name = 'transaction_detail.html'
  
def index(request):    
   
    return HttpResponse(ReturnsLibs.sw.get_order_data('501100'))

def get_order_data(order_id):
    try:
        order_data = ReturnsLibs.sw.get_order_data(order_id)
        if order_data['success']:
            if order_data['totalcount']>0:
                return order_data
            else:
                return None
    except Exception as ex:
        return None


def get_orders(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = Get_orders_form(request.POST)
        # check whether it's valid:
        if form.is_valid():           
            
            request.session["orders_form"] = request.POST.dict()  #save the form as a dict in request.sessions                 
            return redirect('/returns/new_transaction/')     
    # if a GET (or any other method) we'll create a blank form
    else:
        print('request get')
        form = Get_orders_form()
    return render(request, 'get_orders.html', {'form': form})


def insert_new_transaction(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = TransactionForm(request.POST)
        # check whether it's valid:
        print(request.POST)
        if form.is_valid():                           
            # Save a new Article object from the form's data.
            new_transaction = form.save()
            print(new_transaction)
            #return render(request, 'insert.html', {'form': form, 'context': context})
            return redirect('/returns/transactions/{0}'.format(new_transaction.pk))
    # if a GET (or any other method) we'll create a blank form
    else:
        form_data = request.session.pop('orders_form', {})        
        context = {}       
        original_order_id = form_data.get("original_order_id")
        new_order_id = form_data.get('new_order_id')
        original_order_data = get_order_data(original_order_id)
        new_order_data = get_order_data(new_order_id)

        if original_order_data!=None:
            context['original_order_data'] = original_order_data
            if new_order_data!=None:
                context['new_order_data'] = new_order_data     
                form = TransactionForm(
                    initial={
                        'original_order': original_order_id,
                        'new_order': new_order_id,
                        'customer_name': original_order_data['rows'][0]['NAME'],
                        'original_order_payment': original_order_data['rows'][0]['PAYMENT'],
                        'original_amount':original_order_data['rows'][0]['SUMAMNT'],
                        'new_amount':new_order_data['rows'][0]['SUMAMNT']
                        })  
            else:
                form = TransactionForm(
                    initial={
                        'original_order': original_order_id,                        
                        'customer_name': original_order_data['rows'][0]['NAME'],
                        'original_order_payment': original_order_data['rows'][0]['PAYMENT'],
                        'original_amount':original_order_data['rows'][0]['SUMAMNT'],                        
                        })  
        else:
            form = TransactionForm()
        

    return render(request, 'insert.html', {'form': form, 'context':context})

def update_transaction(request, pk):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = TransactionForm(request.POST)
        # check whether it's valid:
        if form.is_valid():                           
            # Save a new Article object from the form's data.
            trans = form.save()
            return redirect('/returns/transactions/')
    else:
        transaction = Transaction.objects.get(pk=pk)
        form = TransactionForm(instance=transaction)

    return render(request, 'insert.html', {'form': form})        