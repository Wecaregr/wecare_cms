from django import forms
from django.forms import ModelForm
from returns.models import Transaction
from django.contrib.auth.models import User

class Get_orders_form(forms.Form):
    original_order_id = forms.CharField(label='Αρχική Παραγγελία', max_length=10)
    new_order_id = forms.CharField(label='Νέα Παραγγελία', max_length=10)

class TransactionForm(ModelForm):
    class Meta:
        model = Transaction
        fields = ['original_order', 'new_order', 'trans_type', 'status', 'customer_name','original_order_payment',
        'original_amount', 'new_amount','description', 'fault_of', 'fault_type']


class MoneyTransfer(ModelForm):
    class Meta:
        model = MoneyTransfer
        fields = ['transaction', 'transfer_account', 'iban', 'bank_name', 'ammount','comments',
        'status', 'transaction_details']
    
    